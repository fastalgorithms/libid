#
# Specify the Fortran compiler FC and its flags FFLAGS.
#

#
# default to GNU fortran, edit make.inc to override

FC = gfortran
FFLAGS = -O2 -w -fPIC -std=legacy

#
# Specify the locations BLAS_LIB and LAPACK_LIB
# of the BLAS and LAPACK archives (libraries).
#
BLAS_LIB = -lopenblas
LAPACK_LIB = -llapack

FLINK = $(BLAS_LIB) $(LAPACK_LIB)

#
# Specify the archiver ARCH and its flags ARCHFLAGS used
# to create the archive (library) .a file.
#
#ARCH = ar
#ARCHFLAGS = -cr

#
# Set RANLIB=echo if your system does not have ranlib.
#
RANLIB = ranlib


# set library names
LIBNAME = libid
DYNAMICLIB = $(LIBNAME).so
DYLIBS = $(BLAS_LIB) $(LAPACK_LIB)

STATICLIB = $(LIBNAME).a

# where the library should be installed
PREFIX = /usr/local

# For your OS, override the above by placing make variables in
# make.inc
-include make.inc



###############################################################################



BIN_DIR = bin
SRC_DIR = src
TMP_DIR = tmp
TEST_DIR = test
#VPATH = $(SRC_DIR):$(BIN_DIR):$(TMP_DIR)

SOURCES = $(SRC_DIR)/dfft.f $(SRC_DIR)/id_rand.f $(SRC_DIR)/id_rtrans.f \
          $(SRC_DIR)/idd_snorm.f $(SRC_DIR)/idd_sfft.f $(SRC_DIR)/idd_frm.f \
          $(SRC_DIR)/idd_house.f $(SRC_DIR)/idd_qrpiv.f $(SRC_DIR)/idd_id.f $(SRC_DIR)/idd_svd.f \
          $(SRC_DIR)/idd_id2svd.f \
          $(SRC_DIR)/iddr_rid.f $(SRC_DIR)/iddr_rsvd.f $(SRC_DIR)/iddp_rid.f \
          $(SRC_DIR)/iddp_rsvd.f \
          $(SRC_DIR)/iddr_aid.f $(SRC_DIR)/iddr_asvd.f $(SRC_DIR)/iddp_aid.f \
          $(SRC_DIR)/iddp_asvd.f \
          $(SRC_DIR)/idz_snorm.f $(SRC_DIR)/idz_sfft.f $(SRC_DIR)/idz_frm.f \
          $(SRC_DIR)/idz_house.f $(SRC_DIR)/idz_qrpiv.f $(SRC_DIR)/idz_id.f \
          $(SRC_DIR)/idz_svd.f \
          $(SRC_DIR)/idz_id2svd.f \
          $(SRC_DIR)/idzr_rid.f $(SRC_DIR)/idzr_rsvd.f $(SRC_DIR)/idzp_rid.f \
          $(SRC_DIR)/idzp_rsvd.f \
          $(SRC_DIR)/idzr_aid.f $(SRC_DIR)/idzr_asvd.f $(SRC_DIR)/idzp_aid.f \
          $(SRC_DIR)/idzp_asvd.f

OBJS = $(patsubst %.f,%.o,$(SOURCES))
TOBJS = $(SRC_DIR)/prini.o

# ID_RAND=id_rand.o prini.o

# ID_RTRANS=id_rtrans.o prini.o id_rand.o

# IDD_SNORM=idd_snorm.o prini.o id_rand.o

# IDD_SFFT=idd_sfft.o prini.o dfft.o id_rand.o

# IDD_HOUSE=idd_house.o prini.o

# IDD_QRPIV=idd_qrpiv.o prini.o idd_house.o
# IDD_ID=idd_id.o prini.o idd_house.o idd_qrpiv.o
# IDD_SVD=idd_svd.o prini.o idd_house.o idd_qrpiv.o id_rand.o $(LAPACK_LIB) \
#         $(BLAS_LIB)
# IDD_ID2SVD=idd_id2svd.o prini.o idd_house.o idd_qrpiv.o idd_id.o id_rand.o \
#            $(LAPACK_LIB) $(BLAS_LIB)
# IDD_FRM=idd_frm.o prini.o dfft.o id_rand.o id_rtrans.o idd_house.o \
#         idd_qrpiv.o idd_id.o idd_sfft.o idd_svd.o $(LAPACK_LIB) $(BLAS_LIB)
# IDDR_RID=iddr_rid.o prini.o idd_house.o idd_qrpiv.o idd_id.o id_rand.o
# IDDR_RSVD=iddr_rsvd.o prini.o idd_house.o idd_qrpiv.o idd_id.o id_rand.o \
#           iddr_rid.o idd_id2svd.o $(LAPACK_LIB) $(BLAS_LIB)
# IDDP_RID=iddp_rid.o prini.o idd_house.o idd_qrpiv.o idd_id.o id_rand.o
# IDDP_RSVD=iddp_rsvd.o prini.o idd_house.o idd_qrpiv.o idd_id.o id_rand.o \
#           iddp_rid.o idd_id2svd.o $(LAPACK_LIB) $(BLAS_LIB)
# IDDR_AID=iddr_aid.o prini.o idd_house.o idd_qrpiv.o idd_id.o id_rand.o \
#          idd_sfft.o id_rtrans.o idd_frm.o dfft.o
# IDDR_ASVD=iddr_asvd.o prini.o idd_house.o idd_qrpiv.o idd_id.o id_rand.o \
#           idd_sfft.o id_rtrans.o idd_frm.o iddr_aid.o idd_id2svd.o \
#           dfft.o $(LAPACK_LIB) $(BLAS_LIB)
# IDDP_AID=iddp_aid.o prini.o idd_house.o idd_qrpiv.o idd_id.o id_rand.o \
#          idd_sfft.o id_rtrans.o idd_frm.o dfft.o
# IDDP_ASVD=iddp_asvd.o prini.o idd_house.o idd_qrpiv.o idd_id.o id_rand.o \
#           idd_sfft.o id_rtrans.o idd_frm.o iddp_aid.o idd_id2svd.o \
#           dfft.o $(LAPACK_LIB) $(BLAS_LIB)
# IDZ_SNORM=idz_snorm.o prini.o id_rand.o
# IDZ_SFFT=idz_sfft.o prini.o dfft.o id_rand.o
# IDZ_HOUSE=idz_house.o prini.o
# IDZ_QRPIV=idz_qrpiv.o prini.o idz_house.o
# IDZ_ID=idz_id.o prini.o idz_house.o idz_qrpiv.o
# IDZ_SVD=idz_svd.o prini.o idz_house.o idz_qrpiv.o id_rand.o $(LAPACK_LIB) \
#         $(BLAS_LIB)
# IDZ_ID2SVD=idz_id2svd.o prini.o idz_house.o idz_qrpiv.o idz_id.o id_rand.o \
#            $(LAPACK_LIB) $(BLAS_LIB)
# IDZ_FRM=idz_frm.o prini.o dfft.o id_rand.o id_rtrans.o idz_house.o \
#         idz_qrpiv.o idz_id.o idz_sfft.o idz_svd.o $(LAPACK_LIB) $(BLAS_LIB)
# IDZR_RID=idzr_rid.o prini.o idz_house.o idz_qrpiv.o idz_id.o id_rand.o
# IDZR_RSVD=idzr_rsvd.o prini.o idz_house.o idz_qrpiv.o idz_id.o id_rand.o \
#           idzr_rid.o idz_id2svd.o $(LAPACK_LIB) $(BLAS_LIB)
# IDZP_RID=idzp_rid.o prini.o idz_house.o idz_qrpiv.o idz_id.o id_rand.o
# IDZP_RSVD=idzp_rsvd.o prini.o idz_house.o idz_qrpiv.o idz_id.o id_rand.o \
#           idzp_rid.o idz_id2svd.o $(LAPACK_LIB) $(BLAS_LIB)
# IDZR_AID=idzr_aid.o prini.o idz_house.o idz_qrpiv.o idz_id.o id_rand.o \
#          idz_sfft.o id_rtrans.o idz_frm.o dfft.o
# IDZR_ASVD=idzr_asvd.o prini.o idz_house.o idz_qrpiv.o idz_id.o id_rand.o \
#           idz_sfft.o id_rtrans.o idz_frm.o idzr_aid.o idz_id2svd.o \
#           dfft.o $(LAPACK_LIB) $(BLAS_LIB)
# IDZP_AID=idzp_aid.o prini.o idz_house.o idz_qrpiv.o idz_id.o id_rand.o \
#          idz_sfft.o id_rtrans.o idz_frm.o dfft.o
# IDZP_ASVD=idzp_asvd.o prini.o idz_house.o idz_qrpiv.o idz_id.o id_rand.o \
#           idz_sfft.o id_rtrans.o idz_frm.o idzp_aid.o idz_id2svd.o \
#           dfft.o $(LAPACK_LIB) $(BLAS_LIB)

IDD_R = $(SRC_DIR)/prini.o $(SRC_DIR)/idd_house.o $(SRC_DIR)/idd_qrpiv.o \
        $(SRC_DIR)/idd_id.o $(SRC_DIR)/id_rand.o $(SRC_DIR)/iddr_rid.o \
        $(SRC_DIR)/iddp_rid.o $(SRC_DIR)/idd_id2svd.o \
        $(SRC_DIR)/iddr_rsvd.o $(SRC_DIR)/iddp_rsvd.o $(SRC_DIR)/idd_snorm.o

IDZ_R = $(SRC_DIR)/prini.o $(SRC_DIR)/idz_house.o $(SRC_DIR)/idz_qrpiv.o \
        $(SRC_DIR)/idz_id.o \
        $(SRC_DIR)/id_rand.o $(SRC_DIR)/idzr_rid.o $(SRC_DIR)/idzp_rid.o \
        $(SRC_DIR)/idz_id2svd.o \
        $(SRC_DIR)/idzr_rsvd.o $(SRC_DIR)/idzp_rsvd.o $(SRC_DIR)/idz_snorm.o

IDD_A = $(SRC_DIR)/prini.o $(SRC_DIR)/idd_house.o $(SRC_DIR)/idd_qrpiv.o \
        $(SRC_DIR)/idd_id.o $(SRC_DIR)/idd_svd.o \
        $(SRC_DIR)/id_rand.o $(SRC_DIR)/idd_sfft.o \
        $(SRC_DIR)/id_rtrans.o $(SRC_DIR)/idd_frm.o $(SRC_DIR)/iddr_aid.o \
        $(SRC_DIR)/iddp_aid.o $(SRC_DIR)/idd_id2svd.o \
        $(SRC_DIR)/iddr_asvd.o \
        $(SRC_DIR)/iddp_asvd.o $(SRC_DIR)/idd_snorm.o $(SRC_DIR)/dfft.o

IDZ_A = $(SRC_DIR)/prini.o $(SRC_DIR)/idz_house.o $(SRC_DIR)/idz_qrpiv.o \
        $(SRC_DIR)/idz_id.o $(SRC_DIR)/idz_svd.o \
        $(SRC_DIR)/id_rand.o $(SRC_DIR)/idz_sfft.o $(SRC_DIR)/id_rtrans.o \
        $(SRC_DIR)/idz_frm.o $(SRC_DIR)/idzr_aid.o \
        $(SRC_DIR)/idzp_aid.o $(SRC_DIR)/idz_id2svd.o $(SRC_DIR)/idzr_asvd.o \
        $(SRC_DIR)/idzp_asvd.o \
        $(SRC_DIR)/idz_snorm.o $(SRC_DIR)/dfft.o

default: usage

usage:
	@echo ""
	@echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo ""
	@echo "Makefile for ID librrary. Specify what to make:"
	@echo "  make install - compile and install the main library"
	@echo "  make install PREFIX=(INSTALL_DIR) - compile and install"
	@echo "       the main library at custom location given by PREFIX"
	@echo "  make lib - compile the library (in lib/ and lib-static/)"
	@echo "  make test - compile and run validation tests"
	@echo "  make objclean - removal all object files, preserving libs"
	@echo "  make clean - also remove libs"
	@echo ""
	@echo "For faster (multicore) making, append the flag -j"
	@echo ""
	@echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo ""

# implicit rules for building .o files
%.o: %.f
	$(FC) -c $(FFLAGS) $< -o $@


# build the library
lib: $(STATICLIB) $(DYNAMICLIB)
	@echo "$(STATICLIB) and $(DYNAMICLIB) built, single-threaded versions"


#id_lib.a:
#	cd $(BIN_DIR); $(ARCH) $(ARCHFLAGS) ../$@ $(OBJS)
#	$(RANLIB) $@


# build and install the library
install: $(STATICLIB) $(DYNAMICLIB)
	echo $(PREFIX)
	mkdir -p $(PREFIX)
	cp -f lib/$(DYNAMICLIB) $(PREFIX)/lib/
	cp -f include/id.h $(PREFIX)/include/
	@echo "libid.so installed successfully to "$(PREFIX)"/lib"
	@echo "id.h installed successfully to "$(PREFIX)"/include"
	@echo ""
	@echo "- - - - - - - - - - - - NOTE - - - - - - - - - - - - - -"
	@echo ""
	@echo "Make sure to include " $(PREFIX) " in the appropriate"
	@echo "path variable:"
	@echo "    LD_LIBRARY_PATH on Linux"
	@echo "    PATH on windows"
	@echo "    DYLD_LIBRARY_PATH on Mac OSX (not needed if default"
	@echo "        installation directory is used"
	@echo "To compile against the header, use -I"$(PREFIX)"/include"
	@echo "To link against the dynamic library, use -L"$(PREFIX)"/lib -lid"
	@echo ""
	@echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo ""


# individual libraries
$(STATICLIB): $(OBJS) 
	ar rcs $(STATICLIB) $(OBJS)
	mv $(STATICLIB) lib-static/

$(DYNAMICLIB): $(OBJS) 
	$(FC) -shared -fPIC $(OBJS) -o $(DYNAMICLIB) $(DYLIBS) 
	mv $(DYNAMICLIB) lib/



clean:
	rm -f $(BIN_DIR)/*
	rm -f $(TMP_DIR)/*
	rm -f $(SRC_DIR)/*.o
	rm -f lib/*
	rm -f lib-static/*


# the important tests
test: idd_r_test idz_r_test idd_a_test idz_a_test

# all possible tests
testall: test id_rand_test id_rtrans_test idd_snorm_test idd_sfft_test \
         idd_house_test idd_qrpiv_test idd_id_test idd_svd_test



idd_r_test: $(IDD_R)
	@echo ""
	@echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo "  Running test for randomized real SVD with matvec"
	$(FC) $(FFLAGS) -o $(BIN_DIR)/$@ $(TEST_DIR)/$@.f $(IDD_R) $(FLINK)
	./$(BIN_DIR)/$@ < size.txt 2>  $(BIN_DIR)/$@_err.out
	mv fort.13 $(BIN_DIR)/$@.fort.13
	@echo Congratulations! $@ is reporting high precision results \
	and no errors.
	@echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo ""


idz_r_test: $(IDZ_R)
	@echo ""
	@echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo "  Running test for randomized complex SVD with matvec"
	$(FC) $(FFLAGS) -o $(BIN_DIR)/$@ $(TEST_DIR)/$@.f $(IDZ_R) $(FLINK)
	./$(BIN_DIR)/$@ < size.txt 2>  $(BIN_DIR)/$@_err.out
	mv fort.13 $(BIN_DIR)/$@.fort.13
	@echo Congratulations! $@ is reporting high precision results \
	and no errors.
	@echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo ""


idd_a_test: $(IDD_A)
	@echo ""
	@echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo "  Running test for randomized real SVD "
	$(FC) $(FFLAGS) -o $(BIN_DIR)/$@ $(TEST_DIR)/$@.f $^ $(FLINK)
	./$(BIN_DIR)/$@ < size.txt 2>  $(BIN_DIR)/$@_err.out
	@echo Congratulations! $@ is reporting high precision results \
	and no errors.
	@echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo ""


idz_a_test: $(IDZ_A)
	@echo ""
	@echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo "  Running test for randomized complex SVD "
	$(FC) $(FFLAGS) -o $(BIN_DIR)/$@ $(TEST_DIR)/$@.f $^ $(FLINK)
	./$(BIN_DIR)/$@ < size.txt 2>  $(BIN_DIR)/$@_err.out
	@echo Congratulations! $@ is reporting high precision results \
	and no errors.
	@echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
	@echo ""


# id_rand_test: $(OBJS)
# 	@echo ""
# 	@echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
# 	@echo "  Running test " $@
# 	$(FC) $(FFLAGS) -o $(BIN_DIR)/$@ $(TEST_DIR)/$@.f $^ $(FLINK)
# 	cd $(BIN_DIR); ./$@


# id_rtrans_test: $(OBJS)
# 	@echo ""
# 	@echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
# 	@echo "  Running test " $@
# 	$(FC) $(FFLAGS) -o $(BIN_DIR)/$@ $(TEST_DIR)/$@.f $^ $(FLINK)
# 	cd $(BIN_DIR); ./$@


# idd_snorm_test: $(IDD_SNORM)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDD_SNORM); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@

# idd_sfft_test: $(IDD_SFFT)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDD_SFFT); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@

# idd_house_test: $(IDD_HOUSE)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDD_HOUSE); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@

# idd_qrpiv_test: $(IDD_QRPIV)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDD_QRPIV); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@

# idd_id_test: $(IDD_ID)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDD_ID); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@

#idd_svd_test: $(IDD_SVD)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDD_SVD); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# idd_id2svd_test: $(IDD_ID2SVD)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDD_ID2SVD); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# idd_frm_test: $(IDD_FRM)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDD_FRM); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# iddr_rid_test: $(IDDR_RID)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDDR_RID); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# iddr_rsvd_test: $(IDDR_RSVD)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDDR_RSVD); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# iddp_rid_test: $(IDDP_RID)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDDP_RID); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# iddp_rsvd_test: $(IDDP_RSVD)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDDP_RSVD); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# iddr_aid_test: $(IDDR_AID)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDDR_AID); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# iddr_asvd_test: $(IDDR_ASVD)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDDR_ASVD); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# iddp_aid_test: $(IDDP_AID)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDDP_AID); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# iddp_asvd_test: $(IDDP_ASVD)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDDP_ASVD); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# idz_snorm_test: $(IDZ_SNORM)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDZ_SNORM); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# idz_sfft_test: $(IDZ_SFFT)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDZ_SFFT); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# idz_house_test: $(IDZ_HOUSE)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDZ_HOUSE); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# idz_qrpiv_test: $(IDZ_QRPIV)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDZ_QRPIV); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# idz_id_test: $(IDZ_ID)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDZ_ID); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# idz_svd_test: $(IDZ_SVD)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDZ_SVD); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# idz_id2svd_test: $(IDZ_ID2SVD)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDZ_ID2SVD); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# idz_frm_test: $(IDZ_FRM)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDZ_FRM); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# idzr_rid_test: $(IDZR_RID)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDZR_RID); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# idzr_rsvd_test: $(IDZR_RSVD)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDZR_RSVD); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# idzp_rid_test: $(IDZP_RID)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDZP_RID); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# idzp_rsvd_test: $(IDZP_RSVD)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDZP_RSVD); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# idzr_aid_test: $(IDZR_AID)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDZR_AID); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# idzr_asvd_test: $(IDZR_ASVD)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDZR_ASVD); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# idzp_aid_test: $(IDZP_AID)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDZP_AID); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@
# idzp_asvd_test: $(IDZP_ASVD)
# 	cd $(BIN_DIR); \
# 	$(FC) $(FFLAGS) -o ../$(TMP_DIR)/$@ ../$(TEST_DIR)/$@.f \
# 	$(IDZP_ASVD); rm -f *_test.o
# 	cd $(TMP_DIR); ./$@








