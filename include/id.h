/*********************************************************************
 *
 * June 26, 2020
 * Mike O'Neil oneil@cims.nyu.edu
 *
 * This is a header file to be included when compiling and calling
 * these routines from C. Only the top level, user-callable routines
 * have been included here for convenience (SVD, ID, QR, etc.)
 *
 *********************************************************************/

#ifndef __ID_H
#define __ID_H
#endif

#ifdef __cplusplus
extern "C" {
#endif
  
  void idz_copycols_(int *m, int *n, double _Complex *a, int *krank,
                     int *list, double _Complex *col);
  
  void idz_frmi_(int *m, int *n, double _Complex *w);
  
  void idzp_aid_(double *eps, int *m, int *n, double _Complex *a,
                 double _Complex *work, int *krank, int *list, double _Complex *proj);

  void idzp_id_(double *eps, int *m, int *n, double _Complex *a, int *krank,
                int *list, double *rnorms);

#ifdef __cplusplus
}
#endif
